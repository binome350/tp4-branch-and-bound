#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int** cout;
void quicksort(int nonVus[], int nbNonVus);
void bubble_sort(int vuesNbVusCourant, int nonVus[], int nbNonVus);

void creeCout(int nbSommets) {
    int coutMin = 10;
    int coutMax = 40;
    int i, j, iseed, it;
    iseed = 1;
    cout = (int**) malloc(nbSommets * sizeof (int*));
    for (i = 0; i < nbSommets; i++) {
        cout[i] = (int*) malloc(nbSommets * sizeof (int));
        for (j = 0; j < nbSommets; j++) {
            if (i == j) cout[i][j] = coutMax + 1;
            else {
                it = 16807 * (iseed % 127773) - 2836 * (iseed / 127773);
                if (it > 0) iseed = it;
                else iseed = 2147483647 + it;
                cout[i][j] = coutMin + iseed % (coutMax - coutMin + 1);
            }
        }
    }
}
int lastView;

int compare(const void * a, const void * b) {
    return cout[lastView][*(int*) a] - cout[lastView][*(int*) b];
}

int permut(int vus[], int nbVus, int nonVus[], int nbNonVus, int longueur, int pcc) {
    /*
     Variable globale :
     - pour tout couple de sommets (i,j), cout[i][j] = cout pour aller de i vers j
     Entree :
     - nonVus[0..nbNonVus-1] = sommets non visites
     - vus[0..nbVus-1] = sommets visites
     Precondition :
     - nbVus > 0 et Vus[0] = 0 (on commence toujours par le sommet 0)
     - longueur = somme des couts des arcs du chemin <vus[0], vus[1], ..., vus[nbVus-1]>
     - pcc = longueur du plus court circuit trouve depuis le premier appel à permut
     Postcondition : Soit C l'ensemble des circuits commencant par Vus[0..nbVus-1] et visitant ensuite chaque sommet de nonVus[0..nbNonVus-1] puis retournant en 0.
         S'il existe un circuit de C de longueur inferieure a borne, alors retourne la longueur du plus petit circuit de C, sinon retourne borne
     */
    // INSEREZ VOTRE CODE ICI !
    //si nonV us est vide alors 
    if (nbNonVus == 0) {
        //printf("%d\n",longueur + cout[vus[nbVus-1]][0]);
        if (pcc < longueur + cout[vus[nbVus - 1]][0]) {
            return pcc;
        } else {
            return longueur + cout[vus[nbVus - 1]][0];
        }
    }

    int l = INT_MAX;
    for (int j = 0; j < nbNonVus; j++) {
        if (l > cout[vus[nbVus - 1]][nonVus[j]]) {
            l = cout[vus[nbVus - 1]][nonVus[j]];
        }
    }
    int sommeLi = l;
    for (int j = 0; j < nbNonVus; j++) {
        int li = cout[nonVus[j]][0];
        for (int k = 0; k < nbNonVus; k++) {
            if (j != k && li > cout[nonVus[j]][nonVus[k]]) {
                li = cout[nonVus[j]][nonVus[k]];
            }
        }
        sommeLi += li;
    }
    
    if (sommeLi + longueur >= pcc) {
        //On rejette cette récursion = on coupe la branche
    } else {
        
        //Copie
        int nonVusNEW[nbNonVus];
        for (int j = 0; j < nbNonVus; j++) {
            nonVusNEW[j] = nonVus[j];
        }
        
        //Tri
        //qsort(nonVusNEW, nbNonVus, sizeof (int), compare);
        bubble_sort(vus[nbVus-1],nonVusNEW, nbNonVus);
        //quicksort(NEWnonVusNEW, nbNonVus);

        //pour chaque sommet s j ∈ nonV us faire
        for (int i = 0; i < nbNonVus; i++) {
            // ==== Ajouter s j à la fin de la liste vus ====
            vus[nbVus] = nonVusNEW[i];
            // ==== enlever s j de l’ensemble non Vus ====
            nonVusNEW[i] = nonVusNEW[nbNonVus - 1];

            int coutDeplacementActuel = cout[vus[nbVus - 1]][vus[nbVus]];
            int coutCourant = longueur + coutDeplacementActuel;

            int pccRecu = permut(vus, nbVus + 1, nonVusNEW, nbNonVus - 1, coutCourant, pcc);
            if (pccRecu < pcc) {
                pcc = pccRecu;
            }

            // ==== Retirer s j de la fin de la liste vus ====
            nonVusNEW[nbNonVus - 1] = nonVusNEW[i];
            // ====remettre s j dans l’ensemble nonV us ====
            nonVusNEW[i] = vus[nbVus];
        }
    }
    
    return pcc;
}

//void quicksort(int *A, int len)

void quicksort(int nonVus[], int nbNonVus) {
    if (nbNonVus < 2) return;

    int pivot = nonVus[nbNonVus / 2];

    int i, j;
    for (i = 0, j = nbNonVus - 1;; i++, j--) {
        while (nonVus[i] < pivot) i++;
        while (nonVus[j] > pivot) j--;

        if (i >= j) break;

        int temp = nonVus[i];
        nonVus[i] = nonVus[j];
        nonVus[j] = temp;
    }

    quicksort(nonVus, i);
    quicksort(nonVus + i, nbNonVus - i);
}

void bubble_sort(int vuesNbVusCourant, int nonVus[], int nbNonVus) {
    //void bubble_sort (int *a, int n) {
    int i, t, s = 1;
    while (s) {
        s = 0;
        for (i = 1; i < nbNonVus; i++) {
            if (cout[vuesNbVusCourant][nonVus[i - 1]] > cout[vuesNbVusCourant][nonVus[i]]) {
                t = nonVus[i];
                nonVus[i] = nonVus[i - 1];
                nonVus[i - 1] = t;
                s = 1;
            }
        }
    }
}

int main() {
    int nbSommets, i, pcc;
    scanf("%d", &nbSommets);
    int vus[nbSommets];
    int nonVus[nbSommets - 1];
    creeCout(nbSommets);
    for (i = 0; i < nbSommets - 1; i++)
        nonVus[i] = i + 1;
    vus[0] = 0;
    pcc = permut(vus, 1, nonVus, nbSommets - 1, 0, INT_MAX);
    printf("%d\n", pcc);
    return 0;
}
