#include <stdio.h>
#include <stdlib.h>

void printSolution(int vus[], int nbVus) {
    /*
     Postcondition : Affiche les valeurs de vus[0..nbVus-1] separees par un espace, suivies par un retour à la ligne
     */
    int i;
    for (i = 0; i < nbVus; i++)
        printf("%d ", vus[i]);
    printf("\n");
}

void permut(int vus[], int nbVus, int nonVus[], int nbNonVus) {
    /*
    Entree :
     - vus[0..nbVus-1] = sommets visites
     - nonVus[0..nbNonVus-1] = sommets non visites
    Precondition : nbVus > 0 et vus[0] = 0 (le tour commence toujours par le sommet 0)
    Postcondition : affiche tous les tours commençant par vus[0..nbVus-1] et se terminant par les sommets de nonVus[0..nbNonVus-1] (dans tous les ordres possibles)
     */
    // INSEREZ VOTRE CODE ICI !

    //si nonV us est vide alors 
    if (nbNonVus == 0) {
        //Afficher les éléments de vus, dans l’ordre de la liste;
        printf("%d",vus[0]);
        for (int i = 1; i < nbVus; i++) {
            printf(" %d",vus[i]);
        }
        printf(" \n");
    }

    //pour chaque sommet s j ∈ nonV us faire
    for (int i = 0; i < nbNonVus; i++) {
        // ==== Ajouter s j à la fin de la liste vus ====
        vus[nbVus]=nonVus[i];
      //  nbVus++;
        // ==== enlever s j de l’ensemble nonV us ====
        nonVus[i] = nonVus[nbNonVus-1];
        //nbNonVus--;
        
        //permut(vus, nonV us)
        permut(vus,nbVus+1,nonVus,nbNonVus-1);
        
        // ==== Retirer s j de la fin de la liste vus ====
       // nbNonVus++;
        nonVus[nbNonVus-1] = nonVus[i];
        // ====remettre s j dans l’ensemble nonV us ====
        //nbVus--;
        nonVus[i]=vus[nbVus];
    }

}

int main() {
    int nbSommets, i;
    scanf("%d", &nbSommets);
    int vus[nbSommets], nonVus[nbSommets - 1];
    vus[0] = 0;
    for (i = 0; i < nbSommets - 1; i++)
        nonVus[i] = i + 1;
    permut(vus, 1, nonVus, nbSommets - 1);
    return 0;
}
